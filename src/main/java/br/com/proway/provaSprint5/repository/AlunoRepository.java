package br.com.proway.provaSprint5.repository;

import br.com.proway.provaSprint5.models.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface AlunoRepository extends JpaRepository<Aluno, Long> {
    ArrayList<Aluno> findAll();
    Aluno findById(long id);
    Aluno save(Aluno aluno);
    void deleteById(long id);

    @Query("SELECT a FROM Aluno a WHERE a.nota >= 70")
    ArrayList<Aluno> findAlunoAprove();
}
