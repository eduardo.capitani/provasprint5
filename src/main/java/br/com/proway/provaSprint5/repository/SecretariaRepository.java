package br.com.proway.provaSprint5.repository;

import br.com.proway.provaSprint5.models.Professor;
import br.com.proway.provaSprint5.models.Secretaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface SecretariaRepository extends JpaRepository<Secretaria, Long> {
    ArrayList<Secretaria> findAll();
    Secretaria findById(long id);
    Secretaria save(Secretaria secretaria);
    void deleteById(long id);

    @Query("SELECT s from Secretaria s WHERE s.alunos >= 100")
    ArrayList<Secretaria> findSecretariaPositi();
}
