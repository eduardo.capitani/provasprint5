package br.com.proway.provaSprint5.repository;

import br.com.proway.provaSprint5.models.Aluno;
import br.com.proway.provaSprint5.models.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface ProfessorRepository extends JpaRepository<Professor, Long> {
    ArrayList<Professor> findAll();
    Professor findById(long id);
    Professor save(Professor professor);
    void deleteById(long id);

    @Query("SELECT p FROM Professor p ORDER BY p.materia")
    ArrayList<Professor> findMateria();
}
