package br.com.proway.provaSprint5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaSprint5Application {

	public static void main(String[] args) {
		SpringApplication.run(ProvaSprint5Application.class, args);
	}

}
