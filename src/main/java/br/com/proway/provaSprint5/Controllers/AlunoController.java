package br.com.proway.provaSprint5.Controllers;

import br.com.proway.provaSprint5.models.Aluno;
import br.com.proway.provaSprint5.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class AlunoController {
    @Autowired
    private AlunoRepository alunoRepository;

    @GetMapping("/alunos")
    public ArrayList<Aluno> getAllAlunos() {
        return alunoRepository.findAll();
    }

    @GetMapping("/aluno/{id}")
    public Aluno getAluno(@PathVariable("id") long id) {
        return alunoRepository.findById(id);
    }


    @PostMapping("/aluno/add")
    public ResponseEntity<Aluno> addAluno(@RequestBody Aluno aluno) {
        Aluno c;
        try{
            c = alunoRepository.save(aluno);
            return new ResponseEntity<>(c, HttpStatus.CREATED);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/aluno/{id}")
    public void deleteAluno(@PathVariable("id") long id){
        alunoRepository.deleteById(id);
    }

    @PutMapping("/aluno/{id}")
    public Aluno atualizarAluno(@PathVariable("id") long id, @RequestBody Aluno aluno){
        aluno.setId(id);
        return alunoRepository.save(aluno);
    }

    @GetMapping("/alunoAprove")
    public ArrayList<Aluno> getAlunoAprove() {
        return alunoRepository.findAlunoAprove();
    }


}