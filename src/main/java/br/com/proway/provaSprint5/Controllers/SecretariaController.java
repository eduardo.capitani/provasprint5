package br.com.proway.provaSprint5.Controllers;

import br.com.proway.provaSprint5.models.Secretaria;
import br.com.proway.provaSprint5.repository.SecretariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class SecretariaController {
    @Autowired
    private SecretariaRepository secretariaRepository;

    @GetMapping("/secretarias")
    public ArrayList<Secretaria> getAllSecretarias() {
        return secretariaRepository.findAll();
    }

    @GetMapping("/secretaria/{id}")
    public Secretaria getSecretaria(@PathVariable("id") long id) {
        return secretariaRepository.findById(id);
    }


    @PostMapping("/secretaria/add")
    public ResponseEntity<Secretaria> addSecretaria(@RequestBody Secretaria secretaria) {
        Secretaria c;
        try{
            c = secretariaRepository.save(secretaria);
            return new ResponseEntity<>(c, HttpStatus.CREATED);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/secretaria/{id}")
    public void deleteSecretaria(@PathVariable("id") long id){
        secretariaRepository.deleteById(id);
    }

    @PutMapping("/secretaria/{id}")
    public Secretaria atualizarSecretaria(@PathVariable("id") long id, @RequestBody Secretaria secretaria){
        secretaria.setId(id);
        return secretariaRepository.save(secretaria);
    }

    @GetMapping("/secretariaPositi")
    public ArrayList<Secretaria> getSecretariaPositi() {
        return secretariaRepository.findSecretariaPositi();
    }


}