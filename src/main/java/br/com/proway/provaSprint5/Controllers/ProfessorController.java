package br.com.proway.provaSprint5.Controllers;

import br.com.proway.provaSprint5.models.Professor;
import br.com.proway.provaSprint5.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class ProfessorController {
    @Autowired
    private ProfessorRepository professorRepository;

    @GetMapping("/professores")
    public ArrayList<Professor> getAllProfessors() {
        return professorRepository.findAll();
    }

    @GetMapping("/professor/{id}")
    public Professor getProfessor(@PathVariable("id") long id) {
        return professorRepository.findById(id);
    }


    @PostMapping("/professor/add")
    public ResponseEntity<Professor> addProfessor(@RequestBody Professor professor) {
        Professor c;
        try{
            c = professorRepository.save(professor);
            return new ResponseEntity<>(c, HttpStatus.CREATED);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/professor/{id}")
    public void deleteProfessor(@PathVariable("id") long id){
        professorRepository.deleteById(id);
    }

    @PutMapping("/professor/{id}")
    public Professor atualizarProfessor(@PathVariable("id") long id, @RequestBody Professor professor){
        professor.setId(id);
        return professorRepository.save(professor);
    }

    @GetMapping("/professorMateria")
    public ArrayList<Professor> getProfessorMateria() {
        return professorRepository.findMateria();
    }


}